var express = require('express'),
  app = express(),
  //port = process.env.PORT || 3000;
  port = process.env.PORT || 3001

var path = require('path');

app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Proyecto Final (FRONT)(Curso Practicioner 4Ed): Cotizador Seguro de Salud en puerto: ' + port);

app.get('/', function(req, res) {
  res.sendFile("index.html", {root: '.'});
})
